extends KinematicBody2D
var speed = 0
var velocity = Vector2()

func _physics_process(_delta):
	velocity = Vector2()
	velocity.x = -1
	velocity = velocity.normalized() * speed
	velocity = move_and_slide(velocity)


func _on_VisibilityNotifier2D_screen_entered():
	speed = 80
