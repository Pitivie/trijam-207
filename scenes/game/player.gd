extends KinematicBody2D
onready var animation_player = $AnimationPlayer

var speed = 0
var screen_size
var velocity = Vector2()

var up_button_is_active = false
var down_button_is_active = false

onready var dash_timer = $DashTimer

func _ready():
	screen_size = get_viewport_rect().size

func _physics_process(delta):
	get_input()
	velocity = move_and_slide(velocity)
	
	position += velocity * delta
	position.y = clamp(position.y, 130, (screen_size.y / 2) - 70)

func get_input():
	velocity = Vector2()
	velocity.x = 1
	if Input.is_action_pressed("ui_down") || down_button_is_active:
		velocity.y += 1
	if Input.is_action_pressed("ui_up") || up_button_is_active:
		velocity.y -= 1
	velocity = velocity.normalized() * speed

func _on_Buttons_up_button_on():
	up_button_is_active = true

func _on_Buttons_up_button_off():
	up_button_is_active = false

func _on_Buttons_down_button_on():
	down_button_is_active = true

func _on_Buttons_down_button_off():
	down_button_is_active = false

func _on_Buttons_dash_pressed():
	dash_timer.start()
	speed = speed * 2

func _on_DashTimer_timeout():
	speed = 80

func happy():
	speed = 0
	animation_player.play("happy")

func reset():
	speed = 80
	animation_player.stop()
