extends Node2D
onready var texture_progress = $TextureProgress
onready var cooldown = $Cooldown
onready var dash_button = $DashButton

signal up_button_on
signal up_button_off
signal down_button_on
signal down_button_off
signal dash_pressed

func _physics_process(delta):
	if texture_progress.value > 0:
		texture_progress.value -= 1

	if texture_progress.value <= 0:
		dash_button.disabled = false

func _on_UpButton_button_down():
	emit_signal("up_button_on")

func _on_UpButton_button_up():
	emit_signal("up_button_off")

func _on_DownButton_button_down():
	emit_signal("down_button_on")

func _on_DownButton_button_up():
	emit_signal("down_button_off")

func _on_DashButton_pressed():
	emit_signal("dash_pressed")
	texture_progress.value = 100
	dash_button.disabled = true
