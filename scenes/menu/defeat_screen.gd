extends CanvasLayer

signal game_started

func _ready():
	visible = false

func _on_Button_pressed():
	emit_signal("game_started")
	visible = false
