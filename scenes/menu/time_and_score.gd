extends CanvasLayer
onready var time = $Time
onready var score_lr = $ScoreLR
onready var timer = $Timer

var minutes = 79
var secondes = 55

func _on_Timer_timeout():
	secondes += 1
	if secondes == 60:
		secondes = 0
		minutes += 1

	if minutes == 80:
		time.modulate = Color("#ff0000")

	if secondes < 10:
		time.text = str(minutes) + " : 0" + str(secondes)
	else :
		time.text = str(minutes) + " : " + str(secondes)

func update_score():
	score_lr.text = "24"

func start():
	timer.start()

func reset():
	minutes = 79
	secondes = 55
	time.text = str(minutes) + " : " + str(secondes)
	time.modulate = Color("#ffffff")
	score_lr.text = "19"
