extends CanvasLayer
onready var audio_stream_player = $AudioStreamPlayer

signal started_game

func win():
	visible = true
	audio_stream_player.play()

func _on_Button_pressed():
	emit_signal("started_game")
	visible = false
	audio_stream_player.stop()
