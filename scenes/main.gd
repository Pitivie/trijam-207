extends Node2D

onready var camera_2d = $Camera2D
onready var field = $Field
onready var player = $Player
onready var defeat_screen = $Menu/DefeatScreen
onready var opponents = $Opponents
onready var victory_screen = $Menu/VictoryScreen
onready var time_and_score = $Menu/TimeAndScore
onready var audio_stream_player = $AudioStreamPlayer

var screen_size
var game_status = 'not_started'

## Init position
var opponents_positions = []
var camera_position = Vector2.ZERO
var player_position = Vector2.ZERO

func _ready():
	screen_size = field.texture.get_width()
	for opponent in opponents.get_children():
		opponents_positions.append(opponent.position)
	camera_position = camera_2d.position
	player_position = player.position

func reset():
	var index = 0
	for opponent in opponents.get_children():
		opponent.position = opponents_positions[index]
		opponent.speed = 0
		index += 1
	camera_2d.position = camera_position
	player.position = player_position
	player.reset()

func _process(_delta):
	if (game_status == 'started'):
		camera_2d.position.x += 2
		camera_2d.position.x = clamp(camera_2d.position.x, 0, screen_size)

func _on_GameOverZone_body_entered(body):
	if (body.name == 'Player'):
		game_status = 'end'
		defeat_screen.visible = true
		player.speed = 0

func start_game():
	time_and_score.reset()
	reset()
	time_and_score.visible = true
	time_and_score.start()
	game_status = 'started'
	audio_stream_player.play()

func _on_StartScreen_game_started():
	start_game()

func _on_DefeatScreen_game_started():
	start_game()

func _on_Victory_body_entered(body):
	victory_screen.win()
	time_and_score.update_score()
	game_status = 'end'
	player.happy()

func _on_VictoryScreen_started_game():
	start_game()
